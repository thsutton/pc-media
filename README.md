PC Media
========

The [PC Media][1] [Drupal][2] module installs and configures file and media
handling functionality as required for sites based on the [PC CMS][3]
installation profile.

[1]: https://bitbucket.org/thsutton/pc-media
[2]: https://drupal.org
[3]: https://bitbucket.org/thsutton/pc-cms
