<?php
/**
 * @file
 * pc_media.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function pc_media_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'file_lock_hook';
  $strongarm->value = 'insert';
  $export['file_lock_hook'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'file_lock_mode';
  $strongarm->value = 'all';
  $export['file_lock_mode'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'file_lock_pattern';
  $strongarm->value = '*';
  $export['file_lock_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'file_lock_regex';
  $strongarm->value = '/.*/';
  $export['file_lock_regex'] = $strongarm;

  return $export;
}
